
public class TennisGame2 implements TennisGame
{
    private static final int _FORTY = 3;
	private static final int _THIRTY = 2;
	private static final int _FIFTEEN = 1;
	private static final int _LOVE = 0;
	public int player1Points = _LOVE;
    public int player2Points = _LOVE;
    

    private String player1Name;
    private String player2Name;

    public TennisGame2(String player1Name, String player2Name) {
        this.player1Name = player1Name;
        this.player2Name = player2Name;
    }

    public String getScore(){
        String score = "";
        if (isTie())
            score = getLiteralForTie();
        if (isDeuce())
            score = getLiteralForDeuce();
        if (isNormal())
            score = getLiteralForNormal();
        if (isAdvantageOver(player1Points,player2Points))
            score = getAdvantagePlayer(player1Name);
        if (isAdvantageOver(player2Points,player1Points))
            score = getAdvantagePlayer(player2Name);
		if (isWinner(player1Points, player2Points))
            score = getWinnerPlayer(player1Name);
        if (isWinner(player2Points, player1Points))
            score = getWinnerPlayer(player2Name);
        return score;
    }

	private String getWinnerPlayer(String playerName) {
		return "Win for "+playerName;
	}

	private String getAdvantagePlayer(String playerName) {
		return "Advantage "+playerName;
	}

	private String getLiteralForNormal() {
		return getLiteral(player1Points) + "-" + getLiteral(player2Points);
	}

	private String getLiteralForDeuce() {
		return "Deuce";
	}

	private String getLiteralForTie() {
		return getLiteral(player1Points)+ "-All";
	}

	private boolean isNormal() {
		return player1Points!=player2Points;
	}
	
	private boolean isWinner(int p1point2, int p2point2) {
		return p1point2>=4 && p2point2>=_LOVE && (p1point2-p2point2)>=_THIRTY;
	}

	private boolean isAdvantageOver(int p1point2, int p2point2) {
		return p1point2 > p2point2 && p2point2 >= _FORTY;
	}

	private boolean isDeuce() {
		return player1Points==player2Points && player1Points>=_FORTY;
	}

	private boolean isTie() {
		return player1Points == player2Points && player1Points <= _FORTY;
	}
	
	private String getLiteral(int playerPoints) {
		String result = "";
		if (playerPoints==_LOVE)
			result = "Love";
		if (playerPoints==_FIFTEEN)
			result = "Fifteen";
		if (playerPoints==_THIRTY)
			result = "Thirty";
		if (playerPoints==_FORTY)
			result = "Forty";
		return result;
	}
    
    public void SetP1Score(int number){
        
        for (int i = 0; i < number; i++)
        {
            P1Score();
        }
            
    }
    
    public void SetP2Score(int number){
        
        for (int i = 0; i < number; i++)
        {
            P2Score();
        }
            
    }
    
    public void P1Score(){
        player1Points++;
    }
    
    public void P2Score(){
        player2Points++;
    }

    public void wonPoint(String player) {
        if (player == "player1")
            P1Score();
        else
            P2Score();
    }
}